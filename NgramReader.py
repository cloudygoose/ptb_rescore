import sys

class NgramReader:
    def __init__(self):
        self.gramsProb = {};
        self.gramsBow = {};
        self.loaded = 0;
        self.vocab = {};

    def loadNgram(self, lmfn): #Clean and load a new ngram, return a vocabulary
        self.gramsProb = {};
        self.gramsBow = {};
        gramNow = 0;
        vocab = []; #ready to infer vocab from unigram
        for line in open(lmfn):
            line = line.replace('\n', '').replace('\t',' ');
            if (line.find('-grams') > 0 & line.find('\\') == 0):
                gramNow = int(line[1]); #another gram level reached
                print >> sys.stderr, ' reading ' + str(gramNow) + 'grams';
            else:
                if (gramNow == 0):
                    continue;
                terms = line.split(' ')
                if (len(terms) == 1): #empty line
                    continue;
                gramT = tuple(terms[1:(1 + gramNow)])

                if (gramNow == 1): #all unigram are added to vocab(to be returned)
                    vocab = vocab + [gramT[0]];
                    self.vocab[gramT[0]] = 1;

                self.gramsProb[gramT] = float(terms[0]);
                if (len(terms) > gramNow + 1): #has bow
                    self.gramsBow[gramT] = float(terms[-1]);
        self.loaded = 1;
        return vocab
    
    def calNgramP(self, gram):
        if (self.loaded == 0):
            print 'error: lm not loaded, exiting...'
            sys.exit(1);

        for i in range(0, len(gram)):
            if not(gram[i] in self.vocab):
                #print >> sys.stderr, 'calNgramP::[', gram[i], '] in ', gram, ' transferred to <unk>...'
                gram=list(gram)
                gram[i] = '<unk>'
                gram=tuple(gram)


        if (gram in self.gramsProb):
            return self.gramsProb[gram];
        if (len(gram) == 1):
            print >> sys.stderr, 'warning: reduced to 1 gram but prob not found, returning -9...'
            print >> sys.stderr, gram
            return -9
        if not (gram[0:-1] in self.gramsBow):
            bowp = 0
        else:
            bowp = self.gramsBow[gram[0:-1]]
        return bowp + self.calNgramP(gram[1:])


def main():
    nr = NgramReader();
         
    print 1 # my code here

if __name__ == "__main__":
    main()

#!/bin/bash

set -x
set -e

ORDER=4
DATAD=./DATA

mkdir -p work

ngram-count -text $DATAD/ptb.train_valid.txt -order ${ORDER} -lm work/lm.o${ORDER} -gt3min 1 -gt4min 1 -kndiscount -interpolate -unk
#ngram -unk -lm lm.o${ORDER} -order ${ORDER} -ppl $DATAD/ptb.test.txt -debug 2 > lm.o${ORDER}.ppl.log
#tail -2 lm.o${ORDER}.ppl.log

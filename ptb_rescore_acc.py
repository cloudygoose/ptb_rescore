import sys
import math
from NgramReader import NgramReader

def main():
    print >> sys.stderr, 'Number of arguments:', len(sys.argv), 'arguments.'
    print >> sys.stderr, 'Argument List:', str(sys.argv)
    if len(sys.argv) <= 1:
        print >> sys.stderr, 'error : sys.argv <= 1 detected,'
        sys.exit(1)
    
    scorefn = sys.argv[1]
    qn = int(sys.argv[2])
    
    len_norm = False
    if (len(sys.argv) > 3):
        print '[ATTENTION]will normalize each log prob by', sys.argv[3]
        len_norm = True
        len_arr = {}
        line_num = 1
        for line in open(sys.argv[3]):
            len_arr[line_num] = int(line)
            line_num = line_num + 1

    numm = 0
    max_m = {}
    ans_m = {}
    line_num = 1
    for line in open(scorefn):
        if (line_num % qn == 1):
            numm = numm + 1
            max_m[numm] = -1000000
        s_now = float(line)
        if len_norm == True:
            s_now = s_now * 1.0 / len_arr[line_num] 
        #print s_now, numm
        if (s_now > max_m[numm]):
            max_m[numm] = s_now
            ans_m[numm] = line_num % qn
            if (ans_m[numm] == 0):
                ans_m[numm] = qn
        line_num = line_num + 1
    right_num = 0

    for i in range(1, numm + 1):
        if (ans_m[i] == 1): #in the ptb_rescore task all correct answer is the first one
            right_num = right_num + 1
    print 'right_num:', right_num, 'all_num', numm, 'acc:', right_num * 1.0 / numm

if __name__ == "__main__":
    main()



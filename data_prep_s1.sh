#!/bin/bash
set -x
mkdir -p gen
QNUM=${QNUM:=10}
for MODE in sss ddd iii sdi; do
    python questionGen.py 1 $QNUM ${MODE} > gen/ptb.test.txt.q${QNUM}rs1_M${MODE}.adds
    cat gen/ptb.test.txt.q${QNUM}rs1_M${MODE}.adds | sed -e 's|</s> ||g' -e 's| </s>||g' > gen/ptb.test.txt.q${QNUM}rs1_M${MODE}
    cat gen/ptb.test.txt.q${QNUM}rs1_M${MODE} | awk ' { print (NF+1) } ' > gen/ptb.test.txt.q${QNUM}rs1_M${MODE}.len
done

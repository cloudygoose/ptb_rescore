#!/bin/bash

set -x
set -e

ORDER=4

source set_root.sh

DATAD=${ROOTD}/ptb/DATA
QNUM=${QNUM:=10}
MODE=${MODE:=sss}

ngram -unk -lm work/lm.o4 -order ${ORDER} -ppl gen/ptb.test.txt.q${QNUM}rs1_M${MODE} -debug 2 > work/ngram_q${QNUM}rs1_M${MODE}.ppl
grep logprob work/ngram_q${QNUM}rs1_M${MODE}.ppl | sed '$d' | awk ' { print $4 } ' > work/ngram_q${QNUM}rs1_M${MODE}.ppl.extract
python ptb_rescore_acc.py work/ngram_q${QNUM}rs1_M${MODE}.ppl.extract ${QNUM}
python ptb_rescore_acc.py work/ngram_q${QNUM}rs1_M${MODE}.ppl.extract ${QNUM} gen/ptb.test.txt.q${QNUM}rs1_M${MODE}.len


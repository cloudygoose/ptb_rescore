import sys
import math
from NgramReader import NgramReader
import random

def main():
    print >> sys.stderr, 'Number of arguments:', len(sys.argv), 'arguments.'
    print >> sys.stderr, 'Argument List:', str(sys.argv)
    if len(sys.argv) <= 1:
        print >> sys.stderr, 'error : sys.argv < 1 detected,'
        sys.exit(1)

    random_seed = int(sys.argv[1]);
    print >> sys.stderr, 'Random seed is:', random_seed
    random.seed(random_seed)
    maxLen = 80
    genNum = int(sys.argv[2]);
    print >> sys.stderr, 'Question number is:', genNum
    mode = sys.argv[3]
    print >> sys.stderr, 'Mode is:', mode

    lmfn = './work/lm.o4'
    tarfn = './DATA/ptb.test.txt.adds'
    print >> sys.stderr, 'target file is:', tarfn
    order = 4
    nr = NgramReader();
    vocab = nr.loadNgram(lmfn);
    print >> sys.stderr, 'len of vocab : ' + str(len(vocab))
    
    for line in open(tarfn):
        line = line.replace('\n', '').replace('  ', ' ')
        tokens = line.split(' ')
        printLine(tokens) #first sample is always the correct answer

        for k in range(1, genNum):
            t_play = list(tokens) #clone the original list
            me = mode[random.randrange(0, len(mode))] #s or d or i
            if me == 's': #substitue operation
                pos = random.randrange(1, len(tokens) - 1) #avoid first and last token
                oriW = tokens[pos]
                noiseW = genNoiseWord([], vocab, oriW)
                t_play[pos] = noiseW
            elif me == 'd': #delete a word
                pos = random.randrange(1, len(tokens) - 1) #pos to delete
                if len(t_play) != 3:
                    t_play.pop(pos)  
            elif me == 'i': #insert a word
                pos = random.randrange(1, len(tokens))  
                noiseW = genNoiseWord([], vocab, '')
                t_play.insert(pos, noiseW) 
            else:
                print >> sys.stderr, 'meet unknown mode, exiting...', me
                sys.exit(1)

            printLine(t_play)     

def printf(format, *args):  
    sys.stdout.write(format % args) 

def getProb(nr, gram):
    return (10 ** nr.calNgramP(gram))

def genNoiseWord(his, vocab, oriW):
    idx = random.randrange(0, len(vocab))
    while (vocab[idx] == "</s>" or vocab[idx] == "<s>" or vocab[idx] == oriW):
        idx = random.randrange(0, len(vocab))
    return vocab[idx]

def printLine(tokens):
    for i in range(0, len(tokens)):
        printf("%s ", tokens[i])
    print

if __name__ == "__main__":
    main()
